#Dockerfile 

FROM python:3

RUN apt-get update && apt-get install -y python3-pip
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install requests
COPY run.py ./run.py